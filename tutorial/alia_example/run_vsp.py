from dust.tools.DUST_tools import *
from time import time
if __name__=='__main__':
    # Create DUST model from VSP degenerate geometry
    vspDegen2DustBasic('alia_DegenGeom.csv', 'test', -0.75, show=True, dup_chck='Full', scale=0.0254)

    # ---------------------------------------------------------------------------------

    # Flight Conditions
    dt = 0.005
    t_steps = 20
    a = -5
    b = 0
    v = 31

    period = dt * t_steps
    vx = v*math.cos(math.radians(b))*math.cos(math.radians(a))
    vz = v*math.cos(math.radians(b))*math.sin(math.radians(a))
    vy = v*math.sin(math.radians(b))

    # ---------------------------------------------------------------------------------
    # Dust Settings
    options = {}
    options['basename'] = './Output/alia'
    options['basename_debug'] = './Debug/alia'
    options['u_inf'] = [vx, vy, vz]
    options['tend'] = period
    options['dt'] = 0.005
    options['dt_out'] = 0.005
    options['dt_debug_out'] = 0.005
    options['n_wake_panels'] = 1
    options['n_wake_particles'] = 300000
    options['particles_box_min'] = [-10, -15, -15]
    options['particles_box_max'] = [30, 15, 15]
    options['FMM'] = False
    options['output_start'] = True
    options['LLdamp'] = 15
    options['BoxLength'] = 10
    options['NBox'] = [4, 3, 3]
    options['OctreeOrigin'] = [-10, -15, -15]
    options['FarFieldRatioSource'] = 30
    options['FarFieldRatioDoublet'] = 30
    options['debug_level'] = 3
    options['NOctreeLevels'] = 6
    options['MinOctreePart'] = 5
    options['MultipoleDegree'] = 2

    # Convergence Settings
    conv_options = {}
    conv_options['conv_type'] = 'steady'
    conv_options['max_runs'] = 10
    conv_options['tol'] = 0.05
    conv_options['scheme'] = 'max'
    conv_options['tstep_num'] = None #"None" to check all iterations of last run to determine convergence
    conv_options['comp_axes'] = [[0,0,0],[0,0,1],[0,0,1],[0,0,1],[0,0,1],[0,0,0],[0,0,0]]


    # ---------------------------------------------------------------------------------

    # Run Dust
    a = Runner(out_dir='dust_out/', live_conv_plot=True)
    a.set_dust_options(options)
    a.set_conv_options(conv_options)
    ans = a.run(conv_error=True)
    a.run_vis('./Postprocessing/alia')

    # ---------------------------------------------------------------------------------

    # Process Results
    case = ans['CaseObj']
    # Wing spanwise lift distribution on component 1
    case.components[1].span_fm('auto', mom_pt=None, frame='global', filename='span_lift', var='Force')
    # Chordwise pressure distribution on component 1
    case.components[1].p_line(4, axis='chord', filename='chord_p')
    # Chordwise surface velocity magnitude distribution on component 1
    case.components[1].surf_vel(4, frame='local', dir_vec='mag', axis='chord', filename='chord_surfvel')
    # Print out resulting force on component 1
    print('Forces on Comp1:')
    print(case.components[1].get_forces(frame='global'))
    # Print out resulting force on all components (separate for each component)
    print('Separate Forces on Components0-6:')
    print(case.get_forces(comp_num=None,frame='global'))
    # Print out resulting force on all components (summed together)
    print('Total Sum of Forces on Components0-6:')
    print(case.get_forces(comp_num=None,frame='global', total=True))
