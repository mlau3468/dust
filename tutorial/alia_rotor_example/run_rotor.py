from dust.tools.DUST_tools import *
if __name__=='__main__':
    # --------------------------------------------------------------------------
    # Flight conditions
    # Uber edgewise CFD case
    r = 1340 #rpm
    rev_div = 40 # Number of timesteps per revolution

    period = 1/r*60
    dt = period/rev_div
    vx = 30.96768
    vy = 0
    vz = -2.538984

    # --------------------------------------------------------------------------
    # Dust settings
    options = {}
    options['basename'] = './Output/alia'
    options['basename_debug'] = './Debug/alia'
    options['u_inf'] = [vx, vy, vz]
    options['tend'] = period
    options['dt'] = dt
    options['dt_out'] = dt
    options['dt_debug_out'] = dt
    options['n_wake_panels'] = 1
    options['n_wake_particles'] = 300000
    options['FMM'] = False
    options['particles_box_min'] = [-4, -4, -8]
    options['particles_box_max'] = [8, 4, 4]
    options['LLdamp'] = 40
    options['BoxLength'] = 4
    options['NBox'] = [3, 2, 3]
    options['OctreeOrigin'] = [-4, -4, -8]
    options['FarFieldRatioSource'] = 20
    options['FarFieldRatioDoublet'] = 20
    options['debug_level'] = 3
    options['output_start'] = True
    options['NOctreeLevels'] = 6
    options['MinOctreePart'] = 5
    options['MultipoleDegree'] = 2
    options['VortexRad'] = 0.1
    options['CutoffRad'] = 0.001
    options['RankineRad'] = 0.1
    options['DoubletThreshold'] = 1e-6
    options['Vortstretch'] = True
    options['Diffusion'] = True
    options['ParticlesRedistribution'] = False
    options['ParticlesRedistributionRatio'] = 3
    options['ViscosityEffects'] = False
    options['TurbulentViscosity'] = False

    # Convergence checker settings
    conv_options = {}
    # Looking for periodic convergence
    conv_options['conv_type'] = 'periodic' 
    # number of timesteps in one period is rev_div, defined above as number of timesteps per revolution
    conv_options['period_tsteps'] = rev_div 
    # Run DUST for maximum 10 periods.
    conv_options['max_periods'] = 10 
    # 2% difference in forces between successive revolutions
    conv_options['tol'] = 0.02 
    # maximum (as opposed to average) difference in forces monitored but be less than 2%.  
    conv_options['scheme'] = 'max' 
    # Check convergence across last 2 simulated periods
    conv_options['period_num'] = 2 
    #component1 z-force, component2 z-force 
    conv_options['comp_axes'] = [[0,0,1],[0,0,1]] 


    # --------------------------------------------------------------------------

    # Run Dust
    a = Runner(out_dir='Analysis/', live_conv_plot=True)
    a.set_dust_options(options)
    a.set_conv_options(conv_options)
    ans = a.run(conv_error=True)
    a.run_vis('./Postprocessing/alia')


    # ------------------------------------------------------------------------------
    # Rotor Analysis
    options = {}
    options['save_figs'] = True
    options['rpm'] = r
    options['div'] = rev_div  # div per round
    options['avg_revs'] = 1 # number of revolutions to average through
    options['skip_rev'] = ans['TotalRuns'] - 1 # Start analysis after this number of revolutions
    #options['skip_rev'] = 6
    options['force_conv'] = 0.224 #N to lb
    options['mom_conv'] = 1/1.36 #Nm to lb-ft
    options['verbose'] = True
    options['rtr_moment_pt'] = [0, 0, 0] # Global frame, point to take rotor mometns about
    options['bld_moment_pt'] = [0, 0, 0] # Blade component frame, point to take blade spanwise moments about

    # direction definition for forces and moments of interest for the rotor. In the global frame
    options['lift_dir'] = [0, 0, 1]
    options['drag_dir'] = [1, 0 ,0]
    options['sideforce_dir'] = [0, 1, 0]
    options['torque_dir'] = [0, 0, -1]
    options['roll_dir'] = [-1, 0, 0]
    options['pitch_dir'] = [0, 1, 0]
    # direction definition for forces and moments of interest for the blade. In the blade componenet local frame
    options['blade_lift_dir'] = [0, 0, 1]
    options['blade_drag_dir'] = [1, 0, 0]
    options['blade_sideforce_dir'] = [0, 1, 0]
    options['blade_bend_dir'] = [1, 0 , 0]
    options['blade_twist_dir'] = [0, 1, 0]
    options['blade_torque_dir'] = [0, 0 ,1]


    rotor_result_analysis('Output/alia', 'Analysis/', options, clear_outdir=False)
